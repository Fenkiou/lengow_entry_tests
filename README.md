Install required packages : `pip install -r requirements.txt`

Prepare the database : `python manage.py migrate`

Retrieve data from Lengow API endpoint : `python manage.py getorders`

Test the application : `python manage.py runserver`

Then go to : [http://127.0.0.1:8000/orders/](http://127.0.0.1:8000/orders/)

API built with django rest framework is reachable at : [http://127.0.0.1:8000/api/](http://127.0.0.1:8000/api/)