import xml.etree.ElementTree as ET

from django.core.management.base import BaseCommand, CommandError
from django.db.utils import IntegrityError
from orders.models import MarketPlace, Order

import requests


class Command(BaseCommand):
    help = 'Retrieve orders from Lengow API'
    api_endpoint = 'http://test.lengow.io/orders-test.xml'

    def handle(self, *args, **options):
        r = requests.get(self.api_endpoint)

        root = ET.fromstring(r.text)

        for order in root.iter('order'):
            marketplace = order[0].text
            id_flux = order[1].text
            marketplace_status = order[2][0].text
            lengow_status = order[2][1].text
            order_id = order[3].text

            m, created = MarketPlace.objects.get_or_create(name=marketplace)

            try:
                o = Order(marketplace=m, id_flux=id_flux,
                          marketplace_status=marketplace_status,
                          lengow_status=lengow_status, order_id=order_id)
                o.save()
            except IntegrityError:
                raise CommandError(('The order n°{} is already present '
                                    'in the database.'.format(order_id)))
