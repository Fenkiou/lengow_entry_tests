from django.db import models


class MarketPlace(models.Model):
    name = models.CharField(max_length=30, unique=True)

    def __str__(self):
        return self.name


class Order(models.Model):
    marketplace = models.ForeignKey(MarketPlace)
    id_flux = models.IntegerField()
    marketplace_status = models.CharField(max_length=30, null=True)
    lengow_status = models.CharField(max_length=30, null=True)
    order_id = models.CharField(max_length=30, unique=True)
