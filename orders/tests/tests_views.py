from django.test import TestCase
from django.core.management import call_command


class IndexViewTest(TestCase):
    def test_index_render_index_template(self):
        response = self.client.get('/orders/')
        self.assertTemplateUsed(response, 'orders/index.html')

    def test_index_has_no_orders(self):
        response = self.client.get('/orders/')
        self.assertContains(response, 'There is no order.')

    def test_index_has_orders(self):
        call_command('getorders')
        response = self.client.get('/orders/')
        self.assertNotContains(response, 'There is no order.')


class DetailViewTest(TestCase):
    def setUp(self):
        call_command('getorders')

    def test_detail_render_detail_template(self):
        response = self.client.get('/orders/1/')
        self.assertTemplateUsed(response, 'orders/detail.html')

    def test_detail_nonexistant_order_return_404(self):
        response = self.client.get('/orders/6/')
        self.assertEqual(response.status_code, 404)
