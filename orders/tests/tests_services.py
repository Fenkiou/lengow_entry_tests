from django.test import TestCase
from django.core.management import call_command
from django.http.response import Http404
from orders.services import get_orders, get_order, search_orders, \
                            update_order, create_order
from orders.models import Order
from orders.forms import OrderForm


class ServicesTest(TestCase):
    def setUp(self):
        call_command('getorders')

    def test_get_orders_return_5_items(self):
        count = Order.objects.all().count()
        self.assertEqual(count, 5)

    def test_get_existing_order_pass(self):
        order = get_order(1)
        self.assertEqual(order.order_id, '111-2222222-3333333')

    def test_get_nonexistant_order_fail(self):
        try:
            order = get_order(6)
        except Http404:
            self.assertRaises(Http404)
