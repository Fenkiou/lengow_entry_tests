from django.core.management import call_command, CommandError
from django.test import TestCase
from django.db.utils import IntegrityError

from orders.models import Order


class GetOrdersTest(TestCase):
    def test_call_command_creates_5_orders(self):
        call_command('getorders')
        count = Order.objects.all().count()
        self.assertEqual(count, 5)

    def test_call_twice_raises_integrity_error(self):
        try:
            call_command('getorders')
            call_command('getorders')
        except (IntegrityError, CommandError):
            self.assertRaises((IntegrityError, CommandError))
