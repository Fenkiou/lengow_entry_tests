from django.shortcuts import render, redirect

from .services import get_orders, get_order, search_orders, update_order, \
                      create_order
from .forms import SearchOrderForm, OrderForm


def index(request):
    form = SearchOrderForm()
    orders = get_orders()

    if request.method == 'POST':
        form = SearchOrderForm(request.POST)
        orders = search_orders(form)

    template = 'orders/index.html'
    context = {
        'orders_list': orders,
        'form': form
    }

    return render(request, template, context)


def detail(request, order_id):
    order = get_order(order_id)
    template = 'orders/detail.html'
    context = {
        'order': order,
    }

    return render(request, template, context)


def edit(request, order_id):
    order = get_order(order_id)
    form = OrderForm(instance=order)

    if request.method == 'POST':
        form = OrderForm(request.POST, instance=order)
        order = update_order(form)

        if order:
            return redirect('detail', order_id=order_id)

    template = 'orders/edit.html'
    context = {
        'order': order,
        'form': form
    }

    return render(request, template, context)


def add(request):
    form = OrderForm()

    if request.method == 'POST':
        form = OrderForm(request.POST)
        order = create_order(form)

        if order:
            return redirect('detail', order.id)

    template = 'orders/add.html'
    context = {
        'form': form
    }

    return render(request, template, context)
