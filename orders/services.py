from django.shortcuts import get_object_or_404

from .models import Order, MarketPlace


def get_orders():
    return Order.objects.all()


def get_order(order_id):
    return get_object_or_404(Order, pk=order_id)


def search_orders(form):
    orders = get_orders()

    if form.is_valid():
        marketplace = form.cleaned_data['marketplace']
        marketplace_status = form.cleaned_data['marketplace_status']
        lengow_status = form.cleaned_data['lengow_status']
        id_flux = form.cleaned_data['id_flux']
        order_id = form.cleaned_data['order_id']

        if marketplace:
            m = MarketPlace.objects.get(name=marketplace)
            orders = orders.filter(marketplace=m)

        if marketplace_status:
            orders = orders.filter(
                marketplace_status__icontains=marketplace_status
            )
        if lengow_status:
            orders = orders.filter(
                lengow_status__icontains=lengow_status
            )
        if id_flux:
            orders = orders.filter(
                id_flux__icontains=id_flux
            )
        if order_id:
            orders = orders.filter(
                order_id__icontains=order_id
            )

    return orders


def update_order(form):
    order = None

    if form.is_valid():
        order = form.save()

    return order


def create_order(form):
    order = None

    if form.is_valid():
        order = form.save()

    return order
