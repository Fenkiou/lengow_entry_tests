from django import forms

from .models import Order


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = '__all__'
        widgets = {
            'id_flux': forms.NumberInput(attrs={'class': 'form-control'}),
            'marketplace_status': forms.TextInput(
                attrs={'class': 'form-control'}),
            'lengow_status': forms.TextInput(attrs={'class': 'form-control'}),
            'order_id': forms.TextInput(attrs={'class': 'form-control'}),
        }


class SearchOrderForm(OrderForm):
    def __init__(self, *args, **kwargs):
        super(SearchOrderForm, self).__init__(*args, **kwargs)
        self.fields['marketplace'].required = False
        self.fields['id_flux'].required = False
        self.fields['marketplace_status'].required = False
        self.fields['lengow_status'].required = False
        self.fields['order_id'].required = False
