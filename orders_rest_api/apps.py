from django.apps import AppConfig


class OrdersRestApiConfig(AppConfig):
    name = 'orders_rest_api'
