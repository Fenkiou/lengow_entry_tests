from orders.models import Order, MarketPlace
from rest_framework import serializers


class OrderSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Order
        fields = ('__all__')


class MarketPlaceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = MarketPlace
        fields = ('__all__')
