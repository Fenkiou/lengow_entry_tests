from orders.models import Order, MarketPlace
from rest_framework import viewsets
from .serializers import OrderSerializer, MarketPlaceSerializer


class OrderViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Order.objects.all()
    serializer_class = OrderSerializer


class MarketPlaceViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = MarketPlace.objects.all()
    serializer_class = MarketPlaceSerializer
